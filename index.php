<?php
try{
    $db = new PDO("mysql:host=localhost;dbname=netology", "root", "", array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
} catch (PDOException $e){
    die("Error: ".$e->getMessage());
}
$flagEdit = false;
$str = '';
$id = 0;
if(!empty($_GET['id']) && !empty($_GET['action'])){
    switch ($_GET['action']) {
        case 'edit':
        $flagEdit = true;
        $id = $_GET['id'];
        $results = $db->query('SELECT description FROM tasks WHERE id='.$id);
        while ($row = $results->fetch(PDO::FETCH_ASSOC)){
            $str = $row['description'];
        }
        break;
        case 'done':
        $check = 'UPDATE tasks SET is_done = 1 WHERE id='.$_GET['id'];
        break;
        case 'delete':
        $check = 'DELETE FROM tasks WHERE id ='.$_GET['id'];
        break;
    }
    if(!$flagEdit){
        $db->query($check);
    }
}
if(!empty($_POST['description']) && $_POST['save'] =='Сохранить'){
            $check = "UPDATE tasks SET description ='".$_POST['description']."' WHERE id=".$id;
            $flagEdit = false;
            $db->query($check);
        }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style>
    table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    table td, table th {
        border: 1px solid #ccc;
        padding: 5px;
    }

    table th {
        background: #eee;
    }
    div {
        padding: 7px;
        padding-right: 20px;
        border: solid 1px black;
        font-family: Verdana, Arial, Helvetica, sans-serif;
        font-size: 13pt;
        background: #E6E6FA;
       }
</style>
</head>
<body>

<h1>Список дел на сегодня</h1>
<div style="float: left">
    <?php
    if(!$flagEdit){
    ?>
    <form method="POST" action="#" name="myform">
        <input type="text" name="description" placeholder="Описание задачи" value="" />
        <input type="submit" name="save" value="Добавить" />
    </form>
    <?php
    }else{
    ?>
    <form method="POST" action="#" name="myform">
        <input type="text" name="description" placeholder="Описание задачи" value="<?= $str ?>" />
        <input type="submit" name="save" value="Сохранить" />
    </form>
    <?php
    }
    ?>
</div>
<?php
    try{
        if (!empty($_POST['description']) && $_POST['save'] =='Добавить'){
            $check = "INSERT INTO tasks (description, date_added)
            VALUES ('".$_POST['description']."', '".date("Y-m-d H:i:s")."')";
            $db->query($check);
        }
        $results = $db->query("SELECT * FROM tasks");
        $error_array = $db->errorInfo();
        if($db->errorCode() != 0000){
            echo "SQL ошибка ".$error_array[2].'<br>';
            die("Error");
        }
    } catch (PDOException $e){
        die("Error: ".$e->getMessage());
    }
?>
    <table>
      <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
    </tr>
    <br><br><br>
<?php
    while ($row = $results->fetch(PDO::FETCH_ASSOC)){
?>
     <tr>
        <td><?= $row['description'] ?></td>
        <td><?= $row['date_added'] ?></td>
        <td><?= ($row['is_done'] == 0) ? "<span style='color: orange'>В процессе</span>" :
             "<span style='color: green'>Выполнено</span>" ?>
        </td>
        <td>
            <a href='?id=<?= $row['id'] ?>&action=edit'>Изменить</a>
            <a href='?id=<?= $row['id'] ?>&action=done'>Выполнить</a>
            <a href='?id=<?= $row['id'] ?>&action=delete'>Удалить</a>
        </td>
        </tr>
<?php
    }
?>
</table>
</body>
</html>